package ep2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class fluxoPF {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final Action action = new SwingAction();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fluxoPF window = new fluxoPF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public fluxoPFl() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setBounds(100, 100, 80, 30);
		frame.getContentPane().add(lblAmplitude);

		textField = new JTextField();
		textField.setBounds(100, 150, 300, 30);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblnguloDeFase = new JLabel("Ângulo de fase da tensão");
		lblnguloDeFase.setBounds(100, 200, 200, 30);
		frame.getContentPane().add(lblnguloDeFase);

		JLabel lblNewLabel = new JLabel("Ângulo de fase da corrente");
		lblNewLabel.setBounds(100, 300, 200, 30);
		frame.getContentPane().add(lblNewLabel);

		textField_1 = new JTextField();
		textField_1.setBounds(100, 250, 300, 30);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(100, 350, 300, 30);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);

		JButton btnContinuar = new JButton("continuara");
		btnContinuar.setAction(action);
		btnContinuar.setBounds(275, 425, 125, 30);
		frame.getContentPane().add(btnContinuar);
	}
	private class SwingAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		public SwingAction() {
			putValue(NAME, "Continuar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	public void setVisible(boolean b) {
	}
}
