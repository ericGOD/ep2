package ep2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JCheckBox;

public class janelaPrincipal {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					janelaPrincipal window = new janelaPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public janelaPrincipal() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("botão 1");
		btnNewButton.setBounds(200, 50, 300, 30);
		frame.getContentPane().add(btnNewButton);

		JButton btnNewButton_3 = new JButton("botão 2");
		btnNewButton_3.setBounds(200, 200, 300, 30);
		frame.getContentPane().add(btnNewButton_3);

		JButton btnNewButton_6 = new JButton("botão 3");
		btnNewButton_6.setBounds(200, 350, 300, 30);
		frame.getContentPane().add(btnNewButton_6);
	}
}
