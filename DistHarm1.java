package ep2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class distHarm1 {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					distHarm1 window = new distHarm1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public distHarm1() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Ordens Harmônicas Impares");
		btnNewButton.setBounds(200, 100, 300, 30);
		frame.getContentPane().add(btnNewButton);

		JButton btnOrdensHarmnicasPares = new JButton("ordens harmônicas Pares");
		btnOrdensHarmnicasPares.setBounds(200, 200, 300, 30);
		frame.getContentPane().add(btnOrdensHarmnicasPares);
	}
}
